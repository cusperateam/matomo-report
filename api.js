const express = require('express');
const mongo = require('mongodb');
const config = require('./config.json');
const Report = require('./fetchReport.js')

const router = express.Router();

async function connectDatabase() {
  const client = await mongo.MongoClient.connect('mongodb+srv://' + config.MONGO.user + ':' + config.MONGO.pass + config.MONGO.cluster, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  return client.db(config.MONGO.db).collection(config.MONGO.collection);
}

function getName(id) {
  let name = "Staging"
  switch(+id) {
    case 1 : name = "Cuspera"; break;
    case 2 : name = "Staging"; break;
    case 5 : name = "Execvision"; break;
    case 6 : name = "SurveySparrow"; break;
    case 7 : name = "Hubilo"; break;
    case 8 : name = "Klenty"; break;
    case 9 : name = "Worxogo"; break;
    case 10 : name = "Moengage"; break;
    case 11 : name = "Qumu"; break;
    case 12 : name = "Streamz"; break;
    default : name = id +"_id_File"; break;
  }
  return name;
}

let report = new Report();

//Get
router.get('/get/config', async (req, res) => {
    const post = await connectDatabase();
    res.send(await post.findOne({ matamoId: req.query.id }));
});

router.get('/get/data', async (req, res) => {
  if(!req.query.type || req.query.type == "" || !req.query.sDate || req.query.sDate == "" || !req.query.eDate || req.query.eDate == "" || !req.query.id || req.query.id == "") {
    return res.send("Params missing : req => id , type, sDate and eDate")
  }
  res.header('Content-Type', 'text/csv');
  res.attachment(getName(req.query.id)+"_"+req.query.type+req.query.sDate+"_"+req.query.eDate+".csv");
  if(req.query.type == "links") {
    report.getOutlinks(req.query.sDate,req.query.eDate,req.query.id).then(data=> {
      return res.send(data);
    })
  } else if(req.query.type == "init") {
    report.getInitCSVData(req.query.sDate,req.query.eDate,req.query.id).then(data=> {
      return res.send(data);
    })
  }
  // const post = await connectDatabase();
  // res.send(await post.findOne({ matamoId: req.query.id }));
});

router.post('/post/config', async (req, res) => {
    const posts = await connectDatabase();
    await posts.updateOne({ matamoId: req.query.id }, {
        $set: {
            config: req.body,
            matamoId: req.query.id,
            name: req.query.name,
            updatedAt: new Date()
        }
    }, { upsert: true });
    res.status(200).send(res.body);
    report.restart(req.query.id+"")
});

router.get('/get/csvs', async (req, res) => {
  // res.header('Content-Type', 'text/csv');
  let sd = req.query.sDate ? req.query.sDate : "2021-08-04";
  let ed = req.query.eDate ? req.query.eDate : "2021-08-15";
  /* [5,9,11,12].forEach(v=> {
    report.getReportsData(sd,ed,req.query.id).then(data=> {
      if(req.query.type== "DT") {
        res.attachment(data[0]+"_DT_"+sd+"_"+ed+".csv");
        res.send(data[1]);
      } else {
        res.attachment(data[0]+"_MOB_"+sd+"_"+ed+".csv");
        res.send(data[2]);
      }
      res.send(report.updateSheet(data[0]+"_DT_"+sd+"_"+ed,data[1],data[2]))
    })
     
  })
  const post = await connectDatabase();
  res.send(await post.findOne({ matamoId: req.query.id })); */
});



module.exports = router;