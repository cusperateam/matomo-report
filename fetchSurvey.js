const axios = require('axios');
const config = require('./config.json');
const ct = require('countries-and-timezones');
var cronJob = require('node-cron');

const mongo = require('mongodb');
const { google } = require("googleapis");
const spreadsheetId = "1WsEkU739ASNfiRukHmBcSe0HBA7EAEXCLS83g8g7w40";

const auth = new google.auth.GoogleAuth({
    keyFile: "keys.json", //the key file
    //url to spreadsheets API
    scopes: "https://www.googleapis.com/auth/spreadsheets", 
});

var jobs = [];
var connect = [];

class Survey {

    getName(id) {
        let name = ""
        switch(+id) {
            case 1 : name = "Cuspera"; break;
            case 2 : name = "Staging"; break;
            case 5 : name = "Execvision"; break;
            case 6 : name = "SurveySparrow"; break;
            case 7 : name = "Hubilo"; break;
            case 8 : name = "Klenty"; break;
            case 9 : name = "Worxogo"; break;
            case 10 : name = "Moengage"; break;
            case 11 : name = "Qumu"; break;
            case 12 : name = "Streamz"; break;
            case 14 : name = "Hivebrite"; break;
            default : name = id +"_id"; break;
        }
        return name;
    }

    getDateString(date) {
        let d = new Date(date);
        return typeof date == "string" ? date : d.getFullYear()+"-"+((d.getMonth()+1).toString().length == 1 ? "0"+(d.getMonth()+1).toString() : (d.getMonth()+1))+"-"+(d.getDate().toString().length == 1 ? "0"+d.getDate().toString() : d.getDate());
    }

    scheduleSurveyData(ids) {
        jobs[0] = cronJob.schedule("0 30 2 * * *", async () => {
            let sd = Date.now() - 86400000;
            this.getSurveyData(ids,this.getDateString(sd),this.getDateString(Date.now()))
        })
        jobs[0].start();
        // let flag = 1;
        //     let fd= 1633631400000;
        //     while(flag) {
        //         let sd = fd;
        //         let ed = new Date(new Date(fd).setDate(new Date(fd).getDate() + 7)).getTime()
        //         this.getSurveyData(ids,this.getDateString(sd),this.getDateString(ed))
        //         fd = new Date(new Date(ed).setDate(new Date(ed).getDate() + 1));
        //         console.log(ids);
        //         flag = !(new Date(ed).getFullYear() > 2021 && new Date(ed).getMonth() > 2);
        //         // flag++
        //         flag = false
        //         fd = fd.getTime();
        //     }
    }

    getSurveyData(ids,sd,ed) {
        let req = [];
        let ind = 0;
        req[ind++] = axios.get("https://cuspera.matomo.cloud/index.php?token_auth=d7f3291b530eb96e8ebf118726cee8c9&format=json&date="+sd+","+ed+"&day=range&idSite=1&module=API&method=Live.getLastVisitsDetails&filter_limit=10000&segment=campaignName==cuspera%20feedback%20survey")
        
        ids.forEach(id=> {
            req[ind++] = axios.get("https://cuspera.matomo.cloud/index.php?token_auth=d7f3291b530eb96e8ebf118726cee8c9&format=json&date="+sd+","+ed+"&day=range&idSite="+id+"&module=API&method=Live.getLastVisitsDetails&filter_limit=10000&segment=eventAction=@Captured;eventCategory=@Email")
            req[ind++] = axios.get("https://cuspera.matomo.cloud/index.php?token_auth=d7f3291b530eb96e8ebf118726cee8c9&format=json&date="+sd+","+ed+"&day=range&idSite="+id+"&module=API&method=Live.getLastVisitsDetails&filter_limit=10000&segment=eventAction=@Captured;eventCategory=@Demo")
        })
        let userMap = new Map();
        let surMap = new Map();
        axios.all(req).then(
            axios.spread((...responses)=> {
                responses.forEach((resp,i)=> {
                    if(i == 0) {
                        this.getServerEvent(resp.data);
                        resp.data.forEach(d=> {
                            let data = d.actionDetails.find(x=> (!x.url.includes("google") && x.url.match(/(\d+|\w+|\-+)*\/report/)) || (x.url.includes("google") && x.url.match(/=(\d+|\w+|\-+)*/)));
                            let id = data ? data.url.includes("google") ? data.url.match(/=(\d+|\w+|\-+)*/)[0].replace("=","") : data.url.match(/(\d+|\w+|\-+)*\/report/)[0].replace("/report","") : null;
                            if(id) {
                                surMap.set(id,d);
                            }
                        })
                    } else
                        resp.data.forEach((d,k)=> {
                            let data = d.actionDetails.find(x=> x.eventCategory && x.eventCategory.includes("ReportSent"));
                                userMap.set(data ? data.eventAction : i + "not-f" +k,d);                            
                        })
                })
            //   this.updateSheet(this.getCSVData(csv,evRes,res,prodN));
            this.getCSVData(userMap, surMap)
            })
          ).catch(errors => {
              // react on errors.
              console.error(errors);
          });
    }

    async getServerEvent(data) {
        let csv = [];
        let time = [];
        const newConn = await this.connectDatabase();
        newConn.findOne({ matamoId: "reportServer"}).then(async resp=> {
            let logTime = resp && resp.csvLoggedAt ? resp.csvLoggedAt : 0;
            data.forEach(d=> {
                if(d.lastActionTimestamp * 1000 > logTime){
                    csv.push([
                        d.visitorId,
                        this.getName(d.idSite),
                        d.actionDetails.filter(x=> x.pageTitle == "ReportCreated")?.length,
                        d.actionDetails.filter(x=> x.pageTitle == "ReportSent")?.length,
                        d.actionDetails.filter(x=> x.pageTitle == "ReportOpen")?.length,
                        d.actionDetails.filter(x=> x.pageTitle == "SurveySent")?.length,
                        d.actionDetails.filter(x=> x.pageTitle == "SurveyOpen_Email")?.length,                
                        d.actionDetails.filter(x=> x.pageTitle == "SurveyOpen_Report")?.length,
                        d.actionDetails.filter(x=> x.pageTitle == "ReportShared")?.length,
                        d.actionDetails.find(x=> x.subtitle && x.subtitle.includes("/app/")) ? d.actionDetails.find(x=> x.subtitle && x.subtitle.includes("/app/")).subtitle.match(/\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b/)?.[0] : 
                        d.actionDetails.find(x=> x.subtitle && x.subtitle.includes("/forms/"))?.subtitle.match(/\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b/)?.[0]
                    ])
                    time.push(d.lastActionTimestamp * 1000)
                }
            })
            csv.length ? this.updateSheet2(csv, Math.max(...time)) : null;
        })
    }

    async getCSVData(usrMap, surMap) {
        let reportIds = Array.from(usrMap.keys());
        let csv = [];
        // let csv = [["User visitorId", "Site name","Report Url", "Report ID", "Email", "Report Open", "Survey Sent", "SurveyOpen via Email", "SurveyOpen via Report","Device","ReportGen ServerDate","ReportGen Timestamp", "Action ServerDate", "Referrer Url","ShowReportPreview", "Browser", "Country", "Region"]];
        let time = [];
        const newConn = await this.connectDatabase();
        newConn.findOne({ matamoId: "report"}).then(async resp=> {
            let logTime = resp && resp.csvLoggedAt ? resp.csvLoggedAt : 0;
            reportIds.forEach(id=> {
                let d = usrMap.get(id);
                let ds = surMap.get(id);
                let ev = d.actionDetails.find(x=> x.eventAction && (x.eventAction == "Captured" && x.eventCategory.includes("Email") || x.eventAction == "EmailCaptured"));
                if((ev && ev.timestamp||d.lastActionTimestamp) * 1000 > logTime) {
                    let dts = [
                        d.visitorId,
                        this.getName(d.idSite),
                        !id.includes("not-f")?"https://www.cuspera.com/app/"+id+"/report":"N/A",
                        !id.includes("not-f") ? id : "N/A",
                        ev.eventName.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi)?.[0] || ev.eventName,
                        d.actionDetails.filter(x=> x.eventAction && (x.eventCategory.includes("Demo") && x.eventAction == "EmailCaptured"))?.length,
                        ds ? ds.actionDetails.filter(x=> x.pageTitle == "ReportOpen")?.length : "N/A",
                        ds ? ds.actionDetails.filter(x=> x.pageTitle == "SurveySent")?.length : "N/A",
                        ds ? ds.actionDetails.filter(x=> x.pageTitle == "SurveyOpen_Email")?.length : "N/A",                
                        ds ? ds.actionDetails.filter(x=> x.pageTitle == "SurveyOpen_Report")?.length : "N/A",
                        d.deviceType,d.serverDate,new Date((ev.timestamp - 19800 - 19800)* 1000).toLocaleString() ,surMap.get(id) ? new Date(surMap.get(id).actionDetails[0].timestamp * 1000).toLocaleString() : "",
                        d.referrerUrl,
                        d.actionDetails.filter(x=> x.eventAction == "ShowReportPreview")?.length,
                        d.browser,d.country,d.region
                    ]
                    if(!dts[4].includes("@cuspera.")) {
                        csv.push(dts)
                    }
                    time.push(ev.timestamp* 1000);
                }
            })
            csv.length ? this.updateSheet(csv, Math.max(...time)) : null;
        });

        // resp.data.map(d=> d.actionDetails.find(x=> x.eventAction && x.eventAction == "EmailCaptured") || d.actionDetails.find(x=> x.eventAction && x.eventAction == "Captured"))
        // .map(x=> x.eventName.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi))
        // .filter(x=> x).map(x=> x[0])
    }

    async updateSheet(data, time) {
        console.log("sheet : ");
        const authClientObject = await auth.getClient();
        const googleSheetsInstance = google.sheets({ version: "v4", auth: authClientObject });
        try {
            googleSheetsInstance.spreadsheets.values.append({
                auth, //auth object
                spreadsheetId, //spreadsheet id
                range: "Report Gen"+"!A:A", //sheet name and range of cells
                valueInputOption: "USER_ENTERED", // The information will be passed according to what the usere passes in as date, number or text
                resource: {
                    values: data,
                },
            }).then(async (res)=> {
                const newConn = await this.connectDatabase();
                newConn.updateOne({ matamoId: "report" }, {
                    $set: { csvLoggedAt: time }
                }, { $multi: true }).then(res=> {
                    console.log("updated data log "+id+" : "+new Date(time));
                    connect[id] && connect[id].isConnected() ? connect[id].close() : null;
                });
            });
            console.log("done");
        } catch (err) {
            console.log('Sheets API Error' + err);
        }
    }

    async updateSheet2(data, time) {
        console.log("sheet : ");
        const authClientObject = await auth.getClient();
        const googleSheetsInstance = google.sheets({ version: "v4", auth: authClientObject });
        try {
            googleSheetsInstance.spreadsheets.values.append({
                auth, //auth object
                spreadsheetId, //spreadsheet id
                range: "ReportServerEvent"+"!A:A", //sheet name and range of cells
                valueInputOption: "USER_ENTERED", // The information will be passed according to what the usere passes in as date, number or text
                resource: {
                    values: data,
                },
            }).then(async (res)=> {
                const newConn = await this.connectDatabase();
                newConn.updateOne({ matamoId: "reportServer" }, {
                    $set: { csvLoggedAt: time }
                }, { $multi: true }).then(res=> {
                    console.log("updated data log "+id+" : "+new Date(time));
                    connect[id] && connect[id].isConnected() ? connect[id].close() : null;
                });
            });
            console.log("done");
        } catch (err) {
            console.log('Sheets API Error' + err);
        }
    }

    async connectDatabase(id = 0) {
        try {
            // connect[id] && connect[id].isConnected() ? connect[id].close() : null;
            connect[id] = connect[id] && connect[id].isConnected() ? connect[id] : await mongo.MongoClient.connect('mongodb+srv://' + config.MONGO.user + ':' + config.MONGO.pass + config.MONGO.cluster, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            });
            return connect[id].db(config.MONGO.db).collection(config.MONGO.collection);
        } catch(e) {
            console.log("Error mongo conn "+id);
            return await this.connectDatabase(id);
        }
    }
}

module.exports = Survey