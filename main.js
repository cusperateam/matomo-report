const express = require('express');
const bodyParser = require('body-parser');
const config = require('./config.json');
const Report = require('./fetchReport.js')
const Config = require('./fetchConfig.js')

const app = express();

//Middleware

app.use(bodyParser.json());

const api = require('./api');
const Survey = require('./fetchSurvey');
app.use('/api', api);

// for production
if (process.env.NODE_ENV === 'production') {
    // vue prod folder
    app.use(express.static(__dirname + '/public/'));
    // SPA
    app.get(/.*/, (req, res) => {
        res.sendFile(__dirname + '/public/index.html');
    });
}

const port = process.env.PORT || config.PORT;

app.listen(port, () => console.log(`Server started on port ${port}`));

let report = new Report();
let configGen = new Config();
let surGen = new Survey();

let matamoIds = ["5", "9", "11", "12", "14"];
// ["5", "6", "7", "8", "9", "10", "11", "12"];

(async function (){
    for(id of matamoIds) {
        report.scheduler(id)
        report.scheduleCSV(id)
    }
    report.scheduleLCP()
    surGen.scheduleSurveyData(["1", "5", "9", "11", "12", "14"]);
    configGen.initConfigReport();
}());
