const axios = require('axios');
const config = require('./config.json');
const ct = require('countries-and-timezones');
var cronJob = require('node-cron');

const mongo = require('mongodb');
const { google } = require("googleapis");
// const spreadsheetId = "1WsEkU739ASNfiRukHmBcSe0HBA7EAEXCLS83g8g7w40";
// const spreadsheetId = "16lBf2-QyGDsY9vVFymjKms6U2ymzVJwOvpY-M3bd1TQ";
const spreadsheetId = "1EVUHgtq5i3l44TI3cnzU8He9BmIwrJ1IfBvydd9pRyM";
const lcpSpreadSheetId = "16lBf2-QyGDsY9vVFymjKms6U2ymzVJwOvpY-M3bd1TQ";

const auth = new google.auth.GoogleAuth({
    keyFile: "keys.json", //the key file
    //url to spreadsheets API
    scopes: "https://www.googleapis.com/auth/spreadsheets", 
});

var jobs = [];
var connect = [];

class Report {

    opt = [];
    cats = ["Initiation_", "LetsGetStarted", "Goals", "Processes", "Industries", "BusinessSettingCapture", 
        "EmailCapture", "MiniMessage", "Exit", "ReportPreview"];
    action = ["ChickletClick"];

    // cats = ["c_Initiation"]

    getDateString(date) {
        let d = new Date(date);
        return typeof date == "string" ? date : d.getFullYear()+"-"+(d.getMonth() +1)+"-"+d.getDate();
    }

    getAction(id) {
        return (id == 14 || id == 12 || id == 9) ? "Initiation" : "ShowChicklet";
    }

    logger(sd,ed,matamoData,id) {
        let action = this.getAction(id);
        return new Promise((res,err)=>{
            const url = "https://cuspera.matomo.cloud/index.php?token_auth="+config.TOKEN+"&format=json&date="+this.getDateString(sd)+","+this.getDateString(ed)+"&period=range&idSite="+matamoData.matamoId+"&filter_limit=10000&module=API&method=Live.getLastVisitsDetails&segment=eventAction=@"+action;
            axios.get(url).then((resp)=> {
                res(this.getFilteredData(resp.data,sd,ed,matamoData));
            })
        })
    }

    getData(sd,ed,id) {
        return new Promise((res,err)=>{
            const url = "https://cuspera.matomo.cloud/index.php?token_auth="+config.TOKEN+"&format=json&date="+this.getDateString(sd)+","+this.getDateString(ed)+"&period=range&idSite="+id+"&filter_limit=10000&module=API&method=Live.getLastVisitsDetails&segment=eventCategory=@Initiation";
            console.log(url);
            axios.get(url).then((resp)=> {
                res(resp.data);
            })
        })
    }

    getInitCSVData(sd,ed,id) {
        return new Promise((resolve,err)=>{
            let self = this;
            let msg = "--"
            let origin = "--"
            this.getData(sd,ed,id).then(resp=> {
                var csv = 'UserID,TimeSpent,Category,Action,Message,Origin,TotalTimeSpent,ServerTime\n';  
                resp.forEach(d=> {
                    d.actionDetails.forEach(a=>{
                        if(a.eventCategory && (self.cats.find(x=> a.eventCategory.includes(x)) || self.action.find(x=> a.eventAction.includes(x)))) {
                            msg = a.eventName.includes("&head") ? a.eventName.substr(6+a.eventName.indexOf("&head="),a.eventName.indexOf("&origin")-6) : "--";
                            origin = a.eventName.includes("&origin") ? a.eventName.substr(8+a.eventName.indexOf("&origin=")) : "--"
                            csv += [d.visitorId, a.timeSpentPretty, a.subtitle.replace("Category:","").replace("Action:",""), msg, origin, d.visitDurationPretty, a.serverTimePretty.replace(",", "-")].join(",")
                            csv+= "\n"
                        }
                    })
                });
                resolve(csv);  
            })
        });
    }

    getOutlinks(sd,ed,id) {
        return new Promise((resolve,err)=>{
            this.getData(sd,ed,id).then(async res=> {
                let d = [];
                res.forEach(x=> {
                    let count = 0;
                    let res = x.actionDetails.filter(a=> a.type=="outlink" || a.type == "download");
                    if(res.length) {
                        d.push([x,res,this.getUrls(x,res)]);
                    }
                });
                resolve(await this.getOutLinkCSV(d));
            });
        });
    }

    getUrls(data,res) {
        let urls = [];
        res.forEach((r,i)=> {
            let url;
            let ind = data.actionDetails.indexOf(r);
            if(ind != -1) {
                while(ind > 0 && ind < data.actionDetails.length) {
                    url = data.actionDetails[ind].type == "action" || data.actionDetails[ind].type == "event" ? data.actionDetails[ind].url : null;
                    url ? ind = -1 : ind--;
                }
                urls.push([i,url])
            }
        })
        return urls;
    }

    getOutLinkCSV(data) {
        let csv = 'UserID,Counts,Urls,TimeSpent,ServerTime,Origin\n';
        data.forEach(d=> {
            if(d && d[1] && d[1].length) {
                d[1].forEach((v,i)=> {
                    csv+= [d[0].visitorId, d[1].length, v.url, v.timeSpentPretty, v.serverTimePretty.replace(",", "-"), d[2].find(x=> x[0] == i) ? d[2].find(x=> x[0] == i)[1] : "NULL"].join(",");
                    csv+="\n";
                })
            }
        })
        return csv;
    }

    getFilteredData(data, sd, ed, matamoData) {
        let d = [];
        let action = this.getAction(matamoData.matamoId);
        if(data && data.length) {
            let t = 0;
            data.map(x=> {
                let utcOffSet = x.countryCode && ct.getCountry(x.countryCode.toUpperCase())
                    ? ct.getTimezone(ct.getCountry(x.countryCode.toUpperCase()).timezones[0]).utcOffset : 0;
                d = d.concat(x.actionDetails.filter(a=> a.eventAction && a.eventAction.includes(action) && (a.timestamp * 1000 - (60000 * utcOffSet)) >= sd && (a.timestamp * 1000 - (60000 * utcOffSet)) < ed && (t < a.timestamp ? t = a.timestamp : true)))
            });
            this.sendReport(sd, ed, d, matamoData)
            return d.length ? {count : d.length, date : t * 1000} : false;
        }
        this.sendReport(sd, ed, d, matamoData)
        return data && data.result == "error" ? data.message : false;
    }
    
    async sendReport(sd, ed, data, matamoData) {
        // if(!data.length) return;
        const url = 'https://slack.com/api/chat.postMessage';
        try {
            axios.post(url, {
                link_names:1,
                channel: '#matomo-reports',
                blocks : [
                    {
                        "type": "header",
                        "text": {
                            "type": "plain_text",
                            "text": "Matomo report for " + matamoData.name,
                            "emoji": true
                        }
                    },
                    {
                        "type": "section",
                        "fields": [
                            {
                                "type": "mrkdwn",
                                "text": (data.length < 100 ? "<!channel>, " : "")+ (matamoData.matamoId == 14 ? "*Flow initiation:*\n" : "*Chicklet shown:*\n") + data.length + " times"
                            },
                            {
                                "type": "mrkdwn",
                                "text": "*Report date range:*\n" + this.getDateString(sd) + "  " + this.getDateString(ed)
                            }
                        ]
                    }
                ]
            }, { headers: { authorization: `Bearer ${config.slackToken}` } });
        } catch(e) {
            console.log("Failed to send report on Slack for " + matamoData.name)
        }
    }

    // getAllOutputs(sd,ed,matamoData) {
    //     let promises = [];
    //     for (let val of ["InitiationMini"]) {
    //         let segment = val.split("_")[0] == "a" ? "eventAction=@"+val.split("_")[1] : "eventCategory=@"+val.split("_")[1];
    //         const url = "https://cuspera.matomo.cloud/index.php?token_auth="+config.TOKEN+"&format=json&date="+this.getDateString(sd)+","+this.getDateString(ed)+"&period=range&idSite="+2+"&module=API&method=Live.getLastVisitsDetails&segment=eventCategory=@Initiation";
    //         promises.push(
    //             axios.get(url).then(res => {
    //                 // console.log(res.data)
    //                 this.opt.push(res.data);
    //             })
    //         )
    //     }
    //     Promise.allSettled(promises).then((res) => this.getOutlinks(this.opt[0]));
    // }

    async connectDatabase(id = 0) {
        try {
            // connect[id] && connect[id].isConnected() ? connect[id].close() : null;
            connect[id] = connect[id] && connect[id].isConnected() ? connect[id] : await mongo.MongoClient.connect('mongodb+srv://' + config.MONGO.user + ':' + config.MONGO.pass + config.MONGO.cluster, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            });
            return connect[id].db(config.MONGO.db).collection(config.MONGO.collection);
        } catch(e) {
            console.log("Error mongo conn "+id);
            return await this.connectDatabase(id);
        }
    }

    async scheduler(id) {
        const conn = await this.connectDatabase();
        conn.findOne({ matamoId: id}).then(async data=> {
            if(data && data.config && data.config.cron && cronJob.validate(data.config.cron)) {
                jobs[id] = cronJob.schedule(data.config.cron, async () => {
                    try {
                        const newConn = await this.connectDatabase(id);
                        newConn.findOne({ matamoId: id}).then(data=> {
                            this.logger(data.loggedAt && false ? data.loggedAt : (Date.now() - 86400000), Date.now(), data, id).then(resp=> {
                                if(resp && typeof resp == "object") {
                                    newConn.updateOne({ _id: data._id }, {
                                        $set: { loggedAt: resp.date }
                                    }, { $multi: true }).then(res=> {
                                        console.log(data.name+" "+ resp.count +" times triggered")
                                        connect[id] && connect[id].isConnected() ? connect[id].close() : null;
                                    });
                                } else {
                                    resp ? console.log(id,resp) : console.log("not exist "+id)
                                }
                            });
                        });
                    } catch(e) {
                        console.log(e)
                    }
                }, {
                    scheduled: true,
                    timezone: "Asia/Kolkata"
                });
                jobs[id].start();
            }else{
                console.log(id," invalid cron")
            }
            connect[0] && connect[0].isConnected() ? connect[0].close() : null;
        });
    }

    restart(id) {
        try {
            console.log("restarting "+id)
            jobs[id].stop();
            connect[id] && connect[id].isConnected() ? connect[id].close() : null;
        } catch(e) {
            console.log("error restarting "+id);
        } finally {
            this.scheduler(id)
        }
    }

    getNewData(sd,ed,id) {
        return new Promise((res,err)=>{
            const url = "https://cuspera.matomo.cloud/index.php?token_auth="+config.TOKEN+"&format=json&date="+this.getDateString(sd)+","+this.getDateString(ed)+"&period=range&idSite="+id+"&filter_limit=10000&module=API&method=Live.getLastVisitsDetails";
            // const url = "https://cuspera.matomo.cloud/index.php?token_auth="+config.TOKEN+"&format=json&date=yesterday&period=day&idSite="+id+"&filter_limit=10000&module=API&method=Live.getLastVisitsDetails";
            axios.get(url).then((resp)=> {
                res(resp.data);
            })
        })
    }

    getHeader() {
        return "Site,Device,visitorId,HomePage,PointerMoved,Showchicklet,ChickletClick,CloseChicklet,LetsGetStarted (click),Exit-Page,ShowGoal,ShowProcess,ShowIndustry,ShowSize,ShowForm,EmailCapture_Captured,ExitIntent-Appear,ExitIntent-Close,RyzeLoaded_New,RyzeLoaded_Return,ScheduleDemo_Click,DemoEmail_Capt,DemoExitIntent-Appear,DemoExitIntent-Close,Anchor-Appear,Anchor-Close,PageCount,serverDate,visitDuration,pageLoadTime,Net_Visit,events,resolution,Country,Region,ReportSent,ShowReportPreview,PopUp 1,SessionTime,SessionHidden_Count,PageUnload"
    }

    getPageCount(data) {
        let count = 0;
        if(data && data.length) {
            let aSet = new Set()
            data.forEach(x=> x.url && x.url!="" ? aSet.add(x.url) : null)
            count = aSet.size;
        }
        return count;
    }

    getCatAct(data) {
        let res = [];
        if(data && data.length) {
            data.forEach(d=> {
                d.eventCategory && d.eventAction ? res.push(d.eventCategory+"-"+d.eventAction) : null;
            })
        }
        return res.length ? res.join(",") : "-";
    }

    async scheduleCSV(id) {
        const newConn = await this.connectDatabase(id);
        newConn.findOne({ matamoId: id+""}).then(async resp=> {
            // let flag = true;
            // let fd= 1638144000000;
            // while(flag) {
            //     let sd = fd;
            //     let ed = new Date(new Date(fd).setDate(new Date(fd).getDate() + 7)).getTime()
            //     this.getReportsData(this.getDateString(sd),this.getDateString(ed),id)
            //     fd = new Date(new Date(ed).setDate(new Date(ed).getDate() + 1));
            //     console.log(id);
            //     flag = false;
            //     fd = fd.getTime();
            // }
            let sd = resp && resp.csvLoggedAt ? resp.csvLoggedAt : Date.now() - 86400000;
            jobs[id+"_c"] = cronJob.schedule("0 30 2 * * *", async () => {
                this.getReportsData(this.getDateString(sd),this.getDateString(Date.now()),id)
                console.log(id);
            })
            jobs[id+"_c"].start();
        });
    }

    getTime(time) {
        time ? time = time.replace("s","") : null;
        return time && time.includes("min") ? 
         +(time.split(" min ")[0]*60) + (+time.split(" min ")[1]) : time ? time : 0;

    }

    getSessionTime(actions) {
        const absDifference = (arr1, arr2) => {
            const res = [];
            for(let i = 0; i < arr1.length; i++){
               const el = arr1[i] && arr2[i] ? Math.abs((arr1[i] || 0) - (arr2[i] || 0)) : 0;
               res[i] = el;
            };
            return res;
        };
        return absDifference(actions.filter(x=> x.eventAction == "ReturningUser").map(x=> x.eventName.match(/=\d+/) ? x.eventName.match(/=\d+/)[0].slice(1) : 0)
        , actions.filter(x=> x.eventAction == "SessionHidden").map(x=> x.eventName.match(/=\d+/) ? x.eventName.match(/=\d+/)[0].slice(1) : 0)
        ).reduce((a, b) => a + b, 0)/1000
    }

    async scheduleLCP(id = "1") {
        const newConn = await this.connectDatabase(id);
        newConn.findOne({ matamoId: id}).then(async resp=> {
            let sd = resp && resp.lcpLoggedAt ? resp.lcpLoggedAt : Date.now() - 86400000;
            jobs[id+"_lc"] = cronJob.schedule("0 31 2 * * *", async () => {
                this.fetchLCPData(this.getDateString(sd),this.getDateString(Date.now()))
            })
            jobs[id+"_lc"].start();
        });
    }

    fetchLCPData(sd,ed) {
        return new Promise((res,err)=>{
            const url = "https://cuspera.matomo.cloud/index.php?token_auth=d7f3291b530eb96e8ebf118726cee8c9&format=json&date="+sd+","+ed+"&day=range&idSite=1&module=API&method=Live.getLastVisitsDetails&filter_limit=10000&segment=eventCategory=@LCP%20candidate";
            axios.get(url).then((resp)=> {
                this.getLCPData(resp.data);
            })
        })
    }

    async getLCPData(temp1, id = "1") {
        const newConn = await this.connectDatabase(id);
        newConn.findOne({ matamoId: "1"}).then(async resp=> {
            let logTime = resp && resp.lcpLoggedAt ? resp.lcpLoggedAt : 0;
            console.log("fetching LCP : "+id);
            let sData = [];
            let time = [];
            temp1.map(d=> d.actionDetails.filter(x=> x.eventCategory && x.eventCategory.includes("LCP")).forEach(z=> {
                if(d.lastActionTimestamp * 1000 > logTime) {
                    let data = z.eventAction ? JSON.parse(z.eventAction) : {};
                    sData.push([
                        z.url.includes("categories") ? "Category" : z.url.includes("products") ? "Product" : z.url.includes("compare") ? "Compare" : z.url.endsWith(".com/") ? "Home" : "Other",
                        z.url.includes("categories") ? z.url.slice(z.url.lastIndexOf("/") + 1) : z.url,
                        data.url,
                        data.lcpElement && typeof data.lcpElement != "string" ? data.lcpElement.join(" ") : data.lcpElement,
                        data.loadTime,data.renderTime,data.startTime,data.size,data.duration,this.getDateString(z.timestamp*1000),d.serverTimePretty,
                        d.deviceType != "Desktop" ? "Mobile" : d.deviceType,
                        d.resolution,d.operatingSystem,d.browser
                    ])
                    time.push(d.lastActionTimestamp * 1000);
                }
            }))
            sData.length ? this.updateLcpSheet(sData,id,Math.max(...time)) : console.log("noData lcp : "+id);
        });

    }

    async getReportsData(sd,ed,id) {
        let dcsv = [];
        let self = this;
        // [this.getHeader().split(",")];
        let time = [];
        const newConn = await this.connectDatabase(id);
        newConn.findOne({ matamoId: id+""}).then(async resp=> {
            let logTime = resp && resp.csvLoggedAt ? resp.csvLoggedAt : 0;
            console.log("fetching csv : "+id);
            // let filters = ["PointerMoved" , "RyzeLoaded", "Close", "ChickletClick", "Click", "Anchor", "ClosePrompt", "WEPopUp_RunningCounter_", "WEPopUp_ActualShownCount_", "ShowChicklet", "UserScroll", "ShowAnchor", "NextMsg", "Close", "Appear", "ReturningUser", "NewUser", "SessionHidden", "PageUnload"]
            this.getNewData(sd,ed,id).then(data=> {
                let action = self.getAction(id);
                data.forEach(d=> {
                    if(d.lastActionTimestamp * 1000 > logTime) {
                        let csv =[d.siteName,d.deviceType != "Desktop" ? "Mobile" : d.deviceType,d.visitorId,d.fingerprint,
                        d.siteName == "Qumu" ? d.actionDetails[0]?.url?.endsWith("region=en") ? "1" : "0" : d.actionDetails[0]?.url?.endsWith("region=default") ? "1" : "0",
                        d.actionDetails.some(x=> x.eventAction == "PointerMoved") ? "1":"0",
                        d.actionDetails.filter(x=> x.eventAction == action)?.length,
                        d.actionDetails.filter(x=> x.eventAction == "ChickletClick")?.length,
                        d.actionDetails.find(x=> x.eventAction == "ChickletClick")?.eventValue,
                        d.actionDetails.filter(x=> x.eventCategory && ((x.eventCategory.includes("_Exit_") && x.eventAction == "Anchor") || 
                            (x.eventCategory.includes("MaybeLater") && x.eventAction == "Close") || (x.eventCategory.includes("ScheduleDemo") && x.eventAction == "Click")))?.length,
                        d.actionDetails.filter(x=> x.eventAction && x.eventAction.includes("Lets Get Started"))?.length,
                        d.actionDetails.filter(x=> x.eventAction && x.eventAction.endsWith("Page")).map(x=> x.eventAction).join(","),
                        d.actionDetails.filter(x=> x.eventAction && x.eventAction.includes("ShowGoal"))?.length,
                        d.actionDetails.filter(x=> x.eventAction && x.eventAction.includes("ShowProcess"))?.length,
                        d.actionDetails.filter(x=> x.eventAction && x.eventAction.includes("ShowIndustry"))?.length,
                        d.actionDetails.filter(x=> x.eventAction && x.eventAction.includes("ShowSize"))?.length,
                        d.actionDetails.filter(x=> x.eventAction && x.eventAction.includes("ShowForm"))?.length,
                        // "NA","NA",
                        // d.actionDetails.filter(x=> x.eventAction == "ShowFullReportCta" || x.eventAction == "ShowReportPreview")?.length,
                        // d.actionDetails.filter(x=> x.eventAction == "GetFullReport")?.length,
                        d.actionDetails.filter(x=> x.eventCategory && x.eventCategory.includes("EmailCapture") && x.eventAction == "Captured")?.length,
                        d.actionDetails.filter(x=> (x.eventCategory && x.eventCategory.includes("ExitIntent")) && (x.eventAction == "Appear" || x.eventAction == "ExitIntent_Appear"))?.length,
                        d.actionDetails.filter(x=> (x.eventCategory && x.eventCategory.includes("ExitIntent")) && (x.eventAction == "Close" || x.eventAction == "ExitIntent_Close"))?.length,
                        d.actionDetails.filter(x=> (x.eventCategory && x.eventCategory.includes("RyzeLoaded")) && x.eventAction == "NewUser")?.length,
                        d.actionDetails.filter(x=> (x.eventCategory && x.eventCategory.includes("RyzeLoaded")) && x.eventAction == "ReturningUser")?.length,
                        d.actionDetails.filter(x=> (x.eventCategory && x.eventCategory.includes("ScheduleDemo")) && x.eventAction == "Click")?.length,
                        d.actionDetails.filter(x=> (x.eventCategory && x.eventCategory.includes("DemoExitIntent")) && x.eventAction == "EmailCaptured")?.length,
                        d.actionDetails.filter(x=> x.eventCategory && x.eventCategory.includes("DemoExitIntent") && x.eventAction == "Appear")?.length,
                        d.actionDetails.filter(x=> x.eventCategory && x.eventCategory.includes("DemoExitIntent") && x.eventAction == "Close")?.length,
                        d.actionDetails.filter(x=> x.eventCategory && x.eventCategory.includes("_Exit_") && (x.eventAction == "Anchor" || x.eventAction.endsWith("Page")))?.length ||
                        d.actionDetails.filter(x=> x.eventCategory && x.eventCategory.includes("MaybeLater") && x.eventAction == "Close")?.length,
                        d.actionDetails.filter(x=> x.eventCategory && x.eventCategory.includes("Anchor") && x.eventAction == "Close")?.length,
                        this.getPageCount(d.actionDetails),d.serverDate,d.serverTimePretty,d.visitDuration,this.getTime(d.actionDetails[0]?.pageLoadTime),
                        Math.max(...d.actionDetails.map(x=> x.eventAction && x.eventValue ? x.eventValue : 0)),
                        d.events,d.resolution,d.country,d.region,
                        d.actionDetails.filter(x=> x.eventCategory && x.eventCategory.includes("ReportSent"))?.length,
                        d.actionDetails.filter(x=> x.eventAction == "ShowReportPreview")?.length,
                        [d.actionDetails.filter(x=> x.eventAction == "WEPopUp_ActualShownCount_1")?.length,
                        d.actionDetails.filter(x=> x.eventAction == "WEPopUp_ActualShownCount_2")?.length,
                        d.actionDetails.filter(x=> x.eventAction == "WEPopUp_ActualShownCount_3")?.length,
                        d.actionDetails.filter(x=> x.eventAction == "WEPopUp_ActualShownCount_4")?.length,
                        d.actionDetails.filter(x=> x.eventAction == "WEPopUp_ActualShownCount_5")?.length].join(","),
                        this.getSessionTime(d.actionDetails),
                        d.actionDetails.filter(x=> x.eventAction == "SessionHidden")?.length,
                        d.actionDetails.filter(x=> x.eventAction == "PageUnload")?.length,d.visitIp,d.browser,
                        d.actionDetails.filter(x=> x.eventName && x.eventName.includes("trace=")).map(x=> x.eventName.substr(x.eventName.lastIndexOf("trace=")+6).replaceAll("WEPopUp_ActualShownCount_","WEPopUpActualShownCount").split("_")).map(x=> x.map(y=> this.getAbbrv(y)).join(",")).join(" | ").slice(-50000)
                    ];
                        dcsv.push(csv);
                        time.push(d.lastActionTimestamp * 1000);
                    }
                })
                dcsv.length ? this.updateSheet(dcsv,id,Math.max(...time)) : console.log("noData : "+id);
            })
        });
    }

    getAbbrv(str) {
        let res = "";
        switch(str) {
            case "RyzeLoaded-RyzeLoaded" : res = "RyzL"; break;
            case "PointerMoved-RyzeLoaded" : res = "PoMo"; break;
            case "DocumentReferrer-RyzeLoaded" : res = "DocRef"; break;
            case "NewUser-RyzeLoaded" : res = "NewU"; break;
            case "WEPopUpActualShownCount1-MiniMessage" : res = "SC1"; break;
            case "WEPopUpActualShownCount2-MiniMessage" : res = "SC2"; break;
            case "WEPopUpActualShownCount3-MiniMessage" : res = "SC3"; break;
            case "WEPopUpActualShownCount4-MiniMessage" : res = "SC4"; break;
            case "WEPopUpActualShownCount5-MiniMessage" : res = "SC5"; break;
            // case "ActualShownCount" : res = ""; break;
            // case "1-MiniMessage" : res = ""; break;
            case "ShowChicklet-MiniMessage" : res = "SC"; break;
            case "ReturningUser-RyzeLoaded" : res = "RetU"; break;
            case "SessionHidden-RyzeLoaded" : res = "SesH"; break;
            case "Close-Anchor" : res = "AncC"; break;
            case "Close-MaybeLater" : res = "MBL"; break;
            case "Close-MobilePrompt" : res = "MobProClo"; break;
            case "NextMsg-MiniMessage" : res = "NxtM"; break;
            case "PageUnload-RyzeLoaded" : res = "PagUn"; break;
            case "OpenPrompt-RyzePrompt" : res = "RyzProOpen"; break;
            case "Close-ExitIntent" : res = "IntC"; break;
            case "ClosePrompt-RyzePrompt" : res = "RyzProClose"; break;
            case "ChickletClick-MiniMessage" : res = "CClik"; break;
            case "Initiation-Initiation" : res = "Init"; break;
            case "WyzFlow-EngagedUser" : res = "WyzUsr"; break;
            default : str = str.split("-");
            res = str.map(x=> x.slice(0,3)).join("");
        }
        return res;
    }

    async updateSheet(data,id,time) {
        console.log("sheet : "+id);
        const authClientObject = await auth.getClient();
        const googleSheetsInstance = google.sheets({ version: "v4", auth: authClientObject });
        try {
            googleSheetsInstance.spreadsheets.values.append({
                auth, //auth object
                spreadsheetId, //spreadsheet id
                range: "Oct28 - present"+"!A:A", //sheet name and range of cells
                valueInputOption: "USER_ENTERED", // The information will be passed according to what the usere passes in as date, number or text
                resource: {
                    values: data,
                },
            }).then(async (res)=> {
                const newConn = await this.connectDatabase(id);
                newConn.updateOne({ matamoId: id+"" }, {
                    $set: { csvLoggedAt: time }
                }, { $multi: true }).then(res=> {
                    console.log("updated data log "+id+ " : " + time + " : "+new Date(time));
                    connect[id] && connect[id].isConnected() ? connect[id].close() : null;
                });
            });
        console.log("done : "+id);
        } catch (err) {
            console.log('Sheets API Error '+id+" : " + err);
        }
        // if ((await googleSheetsInstance.spreadsheets.get({spreadsheetId: spreadsheetId})).data.sheets
        //           .filter(sheet => sheet.properties.title === tabName).length === 0) {
        //           await googleSheetsInstance.spreadsheets.batchUpdate ({ 
        //             spreadsheetId: spreadsheetId, 
        //             resource: {requests: [ {addSheet: {properties: {title: tabName }}}]}})
        //         }
        
    }

    async updateLcpSheet(data,id,time) {
        console.log("sheet lcp : "+id);
        const authClientObject = await auth.getClient();
        const googleSheetsInstance = google.sheets({ version: "v4", auth: authClientObject });
        try {
            googleSheetsInstance.spreadsheets.values.append({
                auth, //auth object
                spreadsheetId : lcpSpreadSheetId, //spreadsheet id
                range: "LCP"+"!A:A", //sheet name and range of cells
                valueInputOption: "USER_ENTERED", // The information will be passed according to what the usere passes in as date, number or text
                resource: {
                    values: data
                },
            }).then(async (res)=> {
                const newConn = await this.connectDatabase(id);
                newConn.updateOne({ matamoId: id+"" }, {
                    $set: { lcpLoggedAt: time }
                }, { $multi: true }).then(res=> {
                    console.log("updated data log "+id+ " : " + time + " : "+new Date(time));
                    connect[id] && connect[id].isConnected() ? connect[id].close() : null;
                });
            });
            console.log("done lcp : "+id);
        } catch (err) {
            console.log('Sheets API Error '+id+" : " + err);
        }
    }

}

module.exports = Report