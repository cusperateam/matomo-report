const axios = require('axios');

var cronJob = require('node-cron');

const { google } = require("googleapis");
const spreadsheetId = "13fveRc-K6N62f__LV9Tk75G1HB5K2yweyyNvAE_L4ec";

const auth = new google.auth.GoogleAuth({
    keyFile: "keys.json", //the key file
    //url to spreadsheets API
    scopes: "https://www.googleapis.com/auth/spreadsheets", 
});

class Config {

    ryzeletMap = new Map();

    initConfigReport() {
        cronJob.schedule("0 30 18 * * *", async () => {
            this.getCompareData();
        })
    }

    getCompareData(prodId = [686,16753,14563,3688], env = "production") {
        this.ryzeletMap.clear();
        // return new Promise((resolve, reject)=> {
          let res = [];
          let evRes = new Map();
          let req = [];
          let prodN = [];
          let csv = [];
          env = env == "production" ? "www" : env;
          let ind = 0;
          prodId.forEach((id)=> {
            req[ind++] = axios.get("https://"+env+".cuspera.com/api/v2/get_ryze_data?product_id=CFProdCode-"+id+"&type=ryze_init");
            req[ind++] = axios.get("https://"+env+".cuspera.com/api/v2/get_ryze_data?product_id=CFProdCode-"+id+"&type=evidences");
          });
          ind = 0;
          axios.all(req).then(
            axios.spread((...responses)=> {
              responses.forEach((resp,i)=> {
                if(i%2 == 0) {
                    prodN.push(resp.data.ryzeOptions?.productName);
                    res = this.getBooleanData(resp.data.ryzeOptions?.productName, resp.data, res);
                } else
                    evRes.set(ind++, resp.data && JSON.stringify(resp.data) != {} ? resp.data : null);
              })
              this.updateSheet(this.getCSVData(csv,evRes,res,prodN));
            })
          ).catch(errors => {
              // react on errors.
              console.error(errors);
          });
        // })
      }
      
    getDisplayName(key) {
        let val = key;
        switch(key){
            case "product" : val = "Product Code"; break;
            case "matomoId" : val = "Matomo ID"; break;
            case "ryzelet" : val = "Is Ryzelet?"; break;
            case "showIntent" : val = "Page Exit Intent"; break;
            case "exitIntent" : val = "Botler Exit Intent"; break;
            case "initOnLoad" : val = "Init on load?"; break;
            case "position" : val = "Widget Position"; break;
            case "trialCta" : val = "Trial or Demo or May be Later"; break;
            case "Default Heads" : val = "Messages: Default"; break;
            case "url_config" : val = "Customization: URL"; break;
            case "utm_campaign" : val = "Customization: UTM Campaign"; break;
            case "keyword" : val = "Customization: Campaign Keywords"; break;
            case "geo_config" : val = "Customization: Geography"; break;
            case "newSplash" : val = "Splash Enabled"; break;
            case "workEmail" : val = "Work Email Check"; break;
            case "ryzeletReport" : val = "Ryzelet Report"; break;
            case "showPreview" : val = "Show Preview"; break;
            case "splashCta" : val = "Splash CTA Message"; break;
            case "splashEV" : val = "Splash Evidences"; break;
            case "bullets" : val = "Splash Accomplishments bullets"; break;
        }
        return val;
    }
      
    getRyzeletRelatedConfigs(id,prop,val) {
        let props = ["newSplash", "ryzeletReport", "showPreview"]
        if(prop == "newSplash") {
            id
        }
        if(props.includes(prop)) {
            val = this.ryzeletMap.get(id) && !val ? "TRUE*" : val ? "TRUE^" : val;
        }
        return val;
      }
      
    isRuled(key, parent) {
        return  key == "product" || key == "ryzelet" || key == "newSplash" ||
            key == "ryzeOptionsshowIntent" || key === "matomoId" || key == "showIntent"
                 || key == "position" || key == "workEmail" || key == "ryzeletReport" || key == "showPreview" || key == "lessIntrusiveExitIntent" || key == "caseStudiesSplash"
                 
    }
      
    getRule(key, parent) {
        return (parent == "miniMessage" && key == "head")
    }
      
    //   var unique = (value, index, self) => {
    //     return self.indexOf(value) === index
    //   }
      
    getBooleanData(id, data, resp, parent, isList = false) {
        if(!parent && data.hasOwnProperty("ryzeOptions")) {
            this.ryzeletMap.set(id,data.ryzeOptions.ryzelet)
        }
        if(data && typeof data == 'object' && !data.hasOwnProperty('length') && (parent == "url_config" || parent == "utm_campaign" || parent == "keyword")) {
            let entries = Array.from(Object.keys(data));
            let arr = [];
            if(entries) {
                entries.forEach((key)=> {
                    arr.push([key, data[key]?.prompt?.miniMessage?.map((x,i)=> i+1 + ". "+x.head)?.join("\n\t")])
                })
                resp.push([id, parent, arr.map(x=> x[0] + " : \n\t" + x[1]).join("\n")]);
            }
        } else if(data && typeof data == 'object' && !data.hasOwnProperty('length') && parent == "geo_config") {
            let entries = Array.from(Object.keys(data.region));
            let arr = [];
            if(entries && data.region[entries[0]].prompt) {
                entries.forEach((key)=> {
                    arr.push([key, data.region[key].prompt?.miniMessage?.map((x,i)=> i+1 + ". "+x.head)?.join("\n\t")])
                })
                resp.push([id, parent, arr.map(x=> x[0] + " : \n\t" + x[1]).join("\n")]);
            }
        }
        else if(data && typeof data == 'object' && !data.hasOwnProperty('length')) {
            let entries = Array.from(Object.keys(data));
            if(entries) {
                entries.forEach((key)=> {
                    if(!isList && this.isRuled(key, parent)) {
                        resp.push([id, key, data[key]])
                    } else if(isList && this.getRule(key, isList)) {
                        resp.push([id, parent, typeof data[key] == "string" ? (data[key]) : data[key]])
                    }
                    resp = this.getBooleanData(id, data[key], resp, key);
                })
            }
        }
        if(data && typeof data == 'object' && !data.hasOwnProperty('length') && parent == "prompt") {
            let val = data.miniMessage.filter((x, i, a) => x.trialCta && a.map((z=> z.trialCta)).indexOf(x.trialCta) == i);
            val && val.length ? resp.push([id, "trialCta", val.map((v,i)=> (val.length > 1 ?(i+1+". "):"")+(v.trialCta == true ? "Schedule a trial" : v.trialCta ? v.trialCta : "Maybe Later")).join("\n")]) : resp.push([id, "trialCta", "Maybe Later"]);
            let con = data.miniMessage.filter((x, i, a) => x.conversational && x.conversational.yes && a.map((z=> x.conversational?.yes)).indexOf(x.conversational.yes) == i);
            con && con.length ? resp.push([id, "splashCta", con.map((v,i)=> (con.length > 1 ?(i+1+". "):"")+v.conversational.yes).join("\n")]) : resp.push([id, "splashCta", "Yes, of course!"]);
        }else if(data && typeof data == 'object' && !data.hasOwnProperty('length') && parent == "engage") {
            resp.push([id, "Default Heads", data.prompt?.miniMessage?.map((x,i)=> i+1 + ". "+x.head)?.join("\n")])
        } 
        // else if(data && typeof data == 'object' && !data.hasOwnProperty('length') && parent == "splashText") {
        //     resp.push([id, "splashCta", data.cta])
        // }
         else if(data && typeof data == 'object' && !data.hasOwnProperty('length') && parent == "splashGat" && data.hasOwnProperty('bullets')) {
          resp.push([id, "bullets", data.bullets.map((v,i)=> i+1+". "+v.replace(/<\/?[^>]+(>|$)/g, "")).join("\n")])
        } 
        else if(data && typeof data == 'object' && !data.hasOwnProperty('length') && parent == "emailCapture" && data.hasOwnProperty('exitIntent')) {
            resp.push([id, "exitIntent", "TRUE"])
          }
        return resp
      }

    getCSVData(csv,evRes,res, prodN) {
        let d = [];
        res.forEach((v)=> {
            !d.includes(v[1]) ? d.push(v[1]) : null  
        });
        let m = new Map();
        prodN.forEach((p,i)=> {
            if(evRes.get(i)){
                let arr = [];
                let entries = Array.from(Object.keys(evRes.get(i)));
                if(entries) {
                    entries.forEach((key)=> {
                        arr.push([key,evRes.get(i)[key].map((x,i)=> i+1 + ". "+x.title)?.join("\n\t")])
                    })
                    m.set(p+"_splashEV",arr.map(x=> x[0] + " : \n\t" + x[1]).join("\n"))
                }
            }
        })
        res.forEach(v=> m.set(v[0]+"_"+v[1], v[2]))
        let heads = ["product", "ryzelet", "showIntent", "exitIntent", "position", "trialCta", "Default Heads", "url_config" , "utm_campaign", "keyword", "geo_config", "newSplash", "bullets", "splashCta", "splashEV", "workEmail", "ryzeletReport", "showPreview", "caseStudiesSplash", "lessIntrusiveExitIntent", "matomoId"]
        d.forEach(x=> !heads.includes(x) ? heads.push(x) : null);
        csv.push(["Name"].concat(heads.map(x=> this.getDisplayName(x))).concat(["Init/Delay (sec)"]))//.join(",");
        // csv+="\n";
        prodN.forEach(p=> {
            let r = [];
            r.push(p)
            heads.forEach(v=> {
                r.push(this.getRyzeletRelatedConfigs(p,v,m.has(p+"_"+v) ? m.get(p+"_"+v) : ""))
            })
            csv.push(r.concat(["5,35,90,150,330"]))//.join(",")
            // csv+="\n"
        });
        return csv;
    }

    getDateString(date) {
        let d = new Date(date);
        return typeof date == "string" ? date : d.getFullYear()+"-"+(d.getMonth() +1)+"-"+d.getDate();
    }

    async updateSheet(data,tabName = "production_") {
        console.log("sheet Config");
        const authClientObject = await auth.getClient();
        const googleSheetsInstance = google.sheets({ version: "v4", auth: authClientObject });
        // const sheets = [name,name.replace("DT", "MOB")];
        // sheets.forEach(async (tabName,ind)=> {
            tabName += this.getDateString(Date.now());
            try {
                console.log(tabName + " Config");
                // Only add sheet if it doesn't already exist
                if ((await googleSheetsInstance.spreadsheets.get({spreadsheetId: spreadsheetId})).data.sheets
                  .filter(sheet => sheet.properties.title === tabName).length === 0) {
                  await googleSheetsInstance.spreadsheets.batchUpdate ({ 
                    spreadsheetId: spreadsheetId, 
                    resource: {requests: [ {addSheet: {properties: {title: tabName }}}]}})
                } 
            } catch (err) {
                console.log('Sheets API Error: ' + err);
            }
            await googleSheetsInstance.spreadsheets.values.append({
                auth, //auth object
                spreadsheetId, //spreadsheet id
                range: tabName+"!A:A", //sheet name and range of cells
                valueInputOption: "USER_ENTERED", // The information will be passed according to what the usere passes in as date, number or text
                resource: {
                    values: data,
                },
            });
            console.log("done Config");
        // })
    }
}

module.exports = Config